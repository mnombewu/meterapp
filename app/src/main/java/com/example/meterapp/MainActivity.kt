package com.example.meterapp

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.example.meterapp.databinding.MainActivityBinding

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}