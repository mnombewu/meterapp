package com.example.meterapp.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.meterapp.ui.main.Constants.INSUFFICIENT_FUNDS
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.math.RoundingMode

class MainViewModel : ViewModel() {
    var changeLiveData: MutableLiveData<BigDecimal> = MutableLiveData()

    fun calculateChange(amountToBePaid: Double, amountToBeCharged: Double) {
        viewModelScope.launch {
            //assumption, input values do not go further than 2 decimal places and no need for precision, but will still use BigDecimal
            val amountPaid: BigDecimal = BigDecimal(amountToBePaid).setScale(2, RoundingMode.HALF_EVEN)
            val amountCharged: BigDecimal = BigDecimal(amountToBeCharged).setScale(2, RoundingMode.HALF_EVEN)
            val change: BigDecimal = amountPaid.subtract(amountCharged)

            when {
                change >= BigDecimal.ZERO -> changeLiveData.postValue(change)
                else -> changeLiveData.postValue(INSUFFICIENT_FUNDS)
            }
        }
    }
}