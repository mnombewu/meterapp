package com.example.meterapp.ui.main

import java.math.BigDecimal

object Constants {
    val INSUFFICIENT_FUNDS: BigDecimal = BigDecimal.valueOf(-1)
}