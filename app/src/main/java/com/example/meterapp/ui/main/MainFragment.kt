package com.example.meterapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.meterapp.R
import com.example.meterapp.databinding.MainFragmentBinding

class MainFragment : Fragment() {
    private val meterViewModel: MainViewModel by viewModels()
    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        meterViewModel.changeLiveData.observe(viewLifecycleOwner) { change ->
            when (change) {
                Constants.INSUFFICIENT_FUNDS -> binding.changeAmountTextView.text = getString(R.string.insufficient_funds)
                else -> binding.changeAmountTextView.text = getString(R.string.currency, change.toPlainString())
            }
        }

        binding.calculateButton.setOnClickListener {
            val amountPaid = binding.amountPaidEditText.text.toString()
            val amountCharged = binding.amountChargedEditText.text.toString()

            //todo: might be good to do some validation on the above, maybe InputFilter
            meterViewModel.calculateChange(amountPaid.toDouble(), amountCharged.toDouble())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}